---
layout: post
title: "수원 매교역 팰루시드 분양가, 분양 정보 및 청약 경쟁률"
toc: true
---

   금번 포스팅은 경기도에서 분양한 '매교역 팰루시드'의 아파트 분양 정보와 청약 경쟁률 및 결과에 대해 알아보도록 하겠습니다.
 

 ■ 매교역 팰루시드
 ​
   ■ 분양 정보

 ​
   매교역 팰루시드는 경기도 수원시 권선구 세류동 817-72번지 일원에 수원 권선113-6구역 주택재개발정비사업에 의해 분양하는 아파트로써 지하2층~지상15층 층 32개동 총 2,178세대 중에서 일반공급 712세대, 특별공급 522세대를 예사 분양하며, 저변 표와 다름없이 전용면적 48㎡, 59㎡, 71㎡,  84㎡, 101㎡의 5가지 평형대로 총 9가지 타입을 공급합니다.
 

   아래의 표는 매교역 팰루시드 공급세대수입니다.
 매교역 팰루시드 공급세대수
   연관 단지의 전용면적 84㎡A 정곡 공급금액은 8.54억 원~8.99억 원입니다. 평형에 따른 동별, 층별 분양가와 계약금, 중도금, 잔금의 납입 일정은 아래와 같습니다.
 매교역 팰루시드 분양 공급금액 및 계약금 중도금, 잔금 납입일정
   이조 아파트는 계약금은 10%이고, 2차에 걸쳐 1차는 계약시 정액 2천만원을 납입하고, 30일 이내에 10%의 여타 금액을 납입합니다. 중도금은 60%로써 6차에 걸쳐 10%씩 납입하며, 잔금 30%로써 입주시 납입합니다. 중도금 대출은 이익금 후불제를 적용합니다.

 

   매교역 팰루시드의 전혀 성죽 및 배치도입니다.
 매교역 팰루시드 단 배치도
 

 ■ 분양 일정
 ​
   연관 단지는 입주 예정일은 2026년 8월 예정입니다. 청약일정은 12월 26일부터 28일까지이며, 당첨자 발표일은 24년 1월 5일입니다. 정당계약은 24년 1월 19일부터 1월 25일까지 7일간에 걸쳐 진행됩니다.
 매교역 팰루시드 당첨자 표발 및 정당계약 분양일정
  ■ 입지 분석
 ​
   매교역 팰루시드의 입지와 환경을 보겠습니다.
 매교역 팰루시드 입지와 교통환경
    연결 단지는 수인분당선 매교역의 초역세권에 위치하고 있어 철도 교통으로는 경기도권에서 소변 좋은 위치라고 할 생명 있습니다. 1정거장 떨어진 수원역을 통해서는 1호선(경부선)과 KTX를 이용할 복 있어 서울은 상의물론 지방으로의 이동도 쉽습니다. 그리고, 아침 추진은 늦지만 수원역에 GTX-C도 예정되어 있기 그리하여 운행 환경은 더더욱 좋아질 예정이기도 하죠.

 

   매교역 인근도 많은 정비 사업이 진행되고 있어 '힐스테이트 푸르지오 수원', '수원센트럴 아이파크자이', '매교역 푸르지오 SK VIEW'등과 같이 메이저 라벨 아파트 대단지를 이루면서 과거의 노후화된 모습은 갈수록 사라지고 있는데요. 그럼에도 불구하고 불구하고 아직은 생존 인프라나 거주환경이 훨 나아졌다고 보기는 일삽시 힘듭니다. 하지만, 이런즉 인프라의 부족을 수원역에서 모조리 채울 고갱이 있을 정도로 롯데마트, 롯데백화점과 수원역의 중심상권 등이 집중되어 있어 생활의 불편함이 없죠.

 

   수원은 자연환경면에서도 우수하기로 유명합니다. 도심 한복판에 있는 팔달산은 물론, 도시와 조화롭게 유지되고 있는 수원화성까지 항시 어우러진 곳이죠.
 ​
 ■ 환경 아파트 금새 비교
 ​
    매교역 팰루시드 심리환경 아파트 단지의 시세를 보겠습니다.

 매교역 팰루시드 처지 아파트 가격
     매교역 팰루시드는 84A타입 짐작 분양가가 약 9억 가망 수준입니다. 그런데, 가까이 단지들과 비교해 보면 얼마 아쉬운 분양가인데요. 매교역을 기준으로 대각선에 위치한 '힐스테이트 푸르지오 수원'은 22년 8월 입주한 2,600여 세대에 달하는 신축 대단지 아파트이고, 수원역과 거리가 있지만 1km 정도로 걸어갈 복판 있는 거리에 있는데요. 33평 어림짐작 근시 실거래 가격이 9.3억 원입니다.
 ​
   매교역 팰루시드 북쪽에 위치한 '매교역 푸르지오 SK VIEW' 단지도 22년 7월 입주한 3,600여 세대 규모의 신축 대단지 아파트인데, 근래 실거래 가격이 9억 원을 기록한 바 있습니다.  유익 단지는 수원고, 수원중 뿐만 아니라 25년 3월 건조 예정인 매교초등학교까지 품고 있는 훈육 환경을 갖추고 있습니다.

 

   한때 떨어진 단지로는 '수원 센트럴 아이파크 자이'가 있는데요. 치아 단지는 23년 7월에 입주한 3,400여 세대 규모의 최신축 대단지 아파트입니다. 매교역과는 다소 거리가 있는 입지이기 때문에 34평 종작 실거래 가격이 7억 서광 수준으로 앞의 2개 단지보다는 중과 낮은 편입니다.
 

   그런데, 매교역 팰루시드는 대단지이기는 그럼에도 불구하고 수원역을 도보로 이용하기는 한때 어렵고, 인근 단지들의 메이저 브랜드에 비해서 네임밸류가 떨어지는 편입니다. 브랜드가 좋은 이유는 사람들이 알아주는 유명세값도 있지만, 그만치 아파트의 완성도가 높아 입주하는 사람들의 높다는 의미이기도 한데요. 그런 단지들이 입지 차이가 크지 않고, 신축이면서 호시 가격이 분양가와 비슷하거나 더욱 낮다면 마땅히 사람들은 그쪽을 보다 선택할 확률이 높겠죠.
 

 ■ 매교역 팰루시드 청약 잔여 및 경쟁률, 접수 건수
 ​
      ■ 청약 시마이 및 경쟁률
 ​
   매교역 팰루시드의 1순위 청약결과를 확인해 보겠습니다.
 매교역 팰루시드 1순위 청약 경쟁률
    매교역 팰루시드는 평균 청약경쟁률 1.68 결심 1로 전 타입이 1순위에서 청약 마감되었습니다. 십중팔구 턱걸이 수준인데요. 가격과 마크 메리트가 단시간 부족함에도 불구하고, 1천여 세대의 물량이 1순위에서 마감되었다는 것은 그럼에도 불구하고 선방했다고 봐야 할 것 같네요. 타입별로는 48A가 3.82 개치 1로 가군 높을 정도로 전체적으로 경쟁률이 큰 의미가 없어보입니다. 그리고, 표에서 보다시피 71B의 경우에는 연관 지역이 아닌 용여 지역까지 순서가 돌아갔다는 점도 얼마 아쉬운 부분이죠.
 ​
      ■ 청약 접수 건수
 ​
    매교역 팰루시드의 타입별 청약 접수 건수를 확인해 보겠습니다.
 매교역 팰루시드 타입별 청약 접수 건수
   총 959가구의 공급세대수 대비 총 2,923건의 청약이 접수되었습니다. 타입별로는 84A타입이 총 1,400여 건으로 청약 접수건의 절반을 차지하고 있고, 다음으로 59C, 71A타입이 뒤를 잇고 있지만, 하나하나 300여 건에 못미치는 수준이기 때문에 접수 건수에 큰 의미는 없어 보입니다.
 

   이상으로 매교역 팰루시드의 일반공급 분양가, 입지, 전두리 아파트 가격 등의 분양 정보와 청약 잉여 및 경쟁률 등에 대해서 알아봤습니다.

 

 아울러 보면 좋은 포스팅 ↓↓↓
 좋은 아파트 고르는 방법
 꽃등 집을 장만할때, 여러가지를 고려하게 됩니다. 직장과의 거리와 교통편, 주변의 대형마트 유무, 유치원을 포함한 아이들의 학군까지 생각해야 될 것이 과시 많습니다. 특히, 보통 주택이 아닌 아파트를 알아볼때는 부근지 처지 뿐만 아니라 아파트의 신모 특징도 벌써 알아둬야 하는데요. 좋은 아파트를 고르는데 있어서 알아야 할 것이 무엇이 있을까요? 금번 포스팅에서는 고개 집으로 아파트를 알아볼때 고려해야 될 부분들을 알아보도록 하겠습니다. 대단지 vs 중소규모 일껏 아파트 단지는 세대수가 많은 대단지일수록 좋습니다. 최소 500세대에서, ...
 moneyblue.tistory.com
 아파트청약의 준비와 당첨 확률 높이는법
 아파트로 내집마련의 수단은 크게 두가지가 있습니다. 기존 아파트를 매매하는 방법도 있고, 새로 지어지는 아파트를 분양받는 방법도 있겠죠. 그리고, 아파트를 분양받기 위해서는 청약을 해야 되는건 당연한 일이겠죠. 그러나, 청약은 후세 내가 아파트를 분양받고 싶다고 이내 신청가능한 것이 아닙니다. 그것에 맞는 단계가 위선 준비되어야 있어야 하죠. 이번 포스팅에서는 아파트 청약을 위해서 미리 준비해야 될 것들에 대해서 알아보도록 하겠습니다. 청약통장 가입 청약을 신청하기 위해서는 주인아저씨 미리감치 해야할 일이 청약통장 가입입니다. 나이에 관계 ...
 moneyblue.tistory.com
 전월세전환율, 반전세와 월세 5% 상판 급여 방법
 이번 포스팅에서는 전월세전환율의 개념과 이를 통한 환산보증금 급여 방법, 더욱이 이즈음 계약갱신청구권에 따른 반전세와 월세의 평판 상한선인 5%가 어떻게 계산되는지에 대해 알아보겠습니다. 전월세전환율과 임차료 면 세기 예전에는 전세나 월세 여인상약 만료로 인해 갱신하게 될때, 집주인 측에서 '요즘 시세가 이러니 이만큼 올려달라'는 식으로 해서 임차인과 합의가 되면 계약이 연장되고, 그렇지 않으면 이사 나가는 단순한 구조였습니다. 하지만, 임대차3법이 시작되면서 상황이 다과 복잡해지기 시작했는데요. 처음에는 집값이 어느 편각 폭등하게 되...
 moneyblue.tistory.com
 23년 11월 통국 시도별 및 대구 미분양아파트
 금번 포스팅에서는 국토교통부 통계누리에서 발표한 23년 11월 골 합국 미분양 아파트 거주 통계자료를 통해서 미분양 지역을 대표하는 대구를 포함한 전국의 미분양아파트 정보를 살펴보고, 시장의 분위기와 앞으로의 흐름을 전망해 보도록 하겠습니다. 지난 달에 발표한 미분양 아파트 자료와 분석은 바닥 링크를 참조해 주세요. 이번 글과 비교해서 보시면 도움이 됩니다. 23년 11월 전국 시도별 및 대구 미분양아파트 23년 11월 전국 시도별 및 대구 미분양아파트 요번 포스팅에서는 국토교통부 통계누리에서 발표한 23년 10월 목표 천하 미분...
 moneyblue.tistory.com
 롯데캐슬 시그니처 새중간 분양가, 분양 데이터 및 청약 경쟁률
 금번 포스팅은 경기도에서 분양한 '롯데캐슬 시그니처 중앙'의 아파트 분양 정보와 청약 경쟁률 ...
 blog.naver.com
 고양 장항 제일풍경채 분양가, 분양 물자 및 청약 경쟁률
 금차 포스팅은 경기도에서 분양한 '고양 장항 제일풍경채'의 아파트 분양 정보와 [매교역 팰루시드](https://the-housing.co.kr) 청약 경쟁률 및...
 blog.naver.com
 청주 동일하이빌 파크레인 2단지 분양가, 분양 재료 및 청약 경쟁률
 이번 포스팅은 충북에서 분양한 '청주 동일하이빌 파크레인 2단지'의 아파트 분양 정보와 청약 ...
 blog.naver.com
 힐스테이트 더욱 운정 분양가, 분양 첩정 및 청약 경쟁률
 금번 포스팅은 경기도에서 분양한 '힐스테이트 더 운정'의 아파트 분양 정보와 청약 경쟁률 및 ...
 blog.naver.com
 파주 운정신도시 우미린 파크힐스 분양가, 분양 정보 및 청약 경쟁률
 요번 포스팅은 경기도에서 분양한 '운정신도시 우미린 파크힐스'의 아파트 분양 정보와 청약 경...
 blog.naver.com
 유용한 생활정보와 부동산, 아파트분양정보, 법인,세금,주식,블로그, 스마트스토어 등 각종 재테크 투자와 과정을 공유하며 부자를 꿈꾸는 블로그
 moneyblue.tistory.com
 

